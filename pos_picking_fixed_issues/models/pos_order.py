# -*- coding: utf-8 -*-
# License, author and contributors information in:
# __openerp__.py file at the root folder of this module.

from odoo import models, fields, api, exceptions, _
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT



class PosOrder(models.Model):
    _inherit = "pos.order"

    def create_picking(self):
        """Create a picking for each order and validate it."""
        Picking = self.env['stock.picking']
        Move = self.env['stock.move']
        StockWarehouse = self.env['stock.warehouse']
        for order in self:
            address = order.partner_id.address_get(['delivery']) or {}
            picking_type = order.picking_type_id
            picking_id = False
            location_id = order.location_id.id
            if order.partner_id:
                destination_id = order.partner_id.property_stock_customer.id
            else:
                if (not picking_type) or (not picking_type.default_location_dest_id):
                    customerloc, supplierloc = StockWarehouse._get_partner_locations()
                    destination_id = customerloc.id
                else:
                    destination_id = picking_type.default_location_dest_id.id
            if picking_type:
                pos_qty = all([x.qty >= 0 for x in order.lines])
                picking_id = Picking.create({
                    'origin': order.name,
                    'partner_id': address.get('delivery', False),
                    'date_done': order.date_order,
                    'picking_type_id': picking_type.id,
                    'company_id': order.company_id.id,
                    'move_type': 'direct',
                    'note': order.note or "",
                    'location_id': location_id if pos_qty else destination_id,
                    'location_dest_id': destination_id if pos_qty else location_id,
                })
                message = _("This transfer has been created from the point of sale session: <a href=# data-oe-model=pos.order data-oe-id=%d>%s</a>") % (order.id, order.name)
                picking_id.message_post(body=message)
                order.write({'picking_id': picking_id.id})
            else:

                for line in order.lines.filtered(lambda l: l.product_id.type in ['product', 'consu']):
                    Move += Move.create({
                        'name': line.name,
                        'product_uom': line.product_id.uom_id.id,
                        'picking_id': picking_id and picking_id.id or False,
                        'picking_type_id': picking_type.id,
                        'product_id': line.product_id.id,
                        'product_uom_qty': abs(line.qty),
                        'state': 'draft',
                        'location_id': location_id if line.qty >= 0 else destination_id,
                        'location_dest_id': destination_id if line.qty >= 0 else location_id,
                    })
            if picking_id:
                picking_id.action_confirm()
                picking_id.force_assign()
                order.set_pack_operation_lot()
                picking_id.action_done()
            elif Move:
                Move.action_confirm()
                Move.force_assign()
                Move.action_done()
        lst=[]
        if 1:

            for order in self:
                for line in order.lines.filtered(lambda l: l.product_id.type in ['product', 'consu']):
                    lst.append(line.product_id.id)

        self.env['pos.config'].sync_order_data_list(lst)
        return True

