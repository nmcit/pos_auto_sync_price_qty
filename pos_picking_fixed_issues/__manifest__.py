# -*- coding: utf-8 -*-

{
    'name': 'pos_picking_fixed_issues',
    'category': 'sales',
    'summary': 'pos_picking_fixed_issues',
    'version': '1.0',
    'description': '''
     pos_picking_fixed_issues
    ''',
    'author': "Nova Minds",
    'website': 'www.nmcit.com',
    'depends': [
        'point_of_sale',
    ],
    'data': [


    ],
    'installable': True,
    'price': 10,
     'currency': 'EUR',
    'license': 'LGPL-3',
    'auto_install': False,
    'application': True,
}
