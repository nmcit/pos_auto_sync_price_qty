# -*- coding: utf-8 -*-

from odoo import fields, models,tools,api
import json


class PosOrder(models.Model):
    _inherit = "pos.order"

    def create_picking(self):
        result = super(PosOrder, self).create_picking()
        lst=[]
        print ('create pickin  from pos')
        if result:
            for order in self:
                for line in order.lines.filtered(lambda l: l.product_id.type in ['product', 'consu']):
                    # self.env['pos.config'].sync_order_data(line.product_id.id)
                    lst.append(line.product_id.id)

        print ('assssssssssssssssssssss',lst)
        self.env['pos.config'].sync_order_data_list(lst)

        return result


class Inventory(models.Model):
    _inherit = 'stock.inventory'
    @api.multi
    def action_done(self):
        print('action_done')
        result = super(Inventory, self).action_done()
        lst=[]
        for line in self.line_ids:
            if line.product_qty != line.theoretical_qty:
                lst.append(line.product_id.id)
        self.env['pos.config'].sync_order_data_list(lst)

        return result
class stockpicking(models.Model):
    _inherit = 'stock.picking'

    @api.multi
    def do_new_transfer(self):
        print ('do_new_transfer')
        result = super(stockpicking, self).do_new_transfer()
        lst=[]

        for each in self.pack_operation_product_ids:
            lst.append(each.product_id.id)
        self.env['pos.config'].sync_order_data_list(lst)

class pos_config(models.Model):
    _inherit = 'pos.config' 

    allow_incoming_orders = fields.Boolean('Allow Auto Sync data', default=True)


    @api.model
    def sync_order_data_list(self, list):
        notifications = []
        if 1:#for product_id in list:
            for pos_session in self.env['pos.session'].search([('state', '!=', 'closed'), ('config_id.allow_incoming_orders', '=', True)]):
                allo_data = self.env['product.product'].with_context(pricelist=pos_session.config_id.pricelist_id.id).search_read([('sale_ok','=',True),('available_in_pos','=',True),('id','in',list)],['display_name', 'list_price','price','pos_categ_id', 'taxes_id', 'barcode', 'default_code',
                     'to_weight', 'uom_id', 'description_sale', 'description',
                     'product_tmpl_id','tracking','qty_available'])
                notifications.append([(self._cr.dbname, "wv_pos_sync_data_sess",pos_session.user_id.id ),{'type':'product','data':allo_data}])

        # print('allo_data',allo_data)
        # print ('notifications',notifications)
        self.env['bus.bus'].sendmany(notifications)
    @api.model
    def sync_order_data(self, product_id):
        notifications = []
        # for pos_session in self.env['pos.session'].search(
        #         [('state', '!=', 'closed'), ('config_id.allow_incoming_orders', '=', True)]):
        #
        #     allo_data = self.env['product.uom.price'].with_context(pricelist=pos_session.config_id.pricelist_id.id).search_read([('product_id','=',product_id)],['price', 'ration'])
        #     notifications.append([(self._cr.dbname, "wv_pos_sync_data_sess", pos_session.user_id.id),{'type':'product','data':allo_data}])
        # self.env['bus.bus'].sendmany(notifications)
        for pos_session in self.env['pos.session'].search([('state', '!=', 'closed'), ('config_id.allow_incoming_orders', '=', True)]):
            allo_data = self.env['product.product'].with_context(pricelist=pos_session.config_id.pricelist_id.id).search_read([('sale_ok','=',True),('available_in_pos','=',True),('id','=',product_id)],['display_name', 'list_price','price','pos_categ_id', 'taxes_id', 'barcode', 'default_code',
                 'to_weight', 'uom_id', 'description_sale', 'description',
                 'product_tmpl_id','tracking','qty_available'])
            notifications.append([(self._cr.dbname, "wv_pos_sync_data_sess", pos_session.user_id.id),{'type':'product','data':allo_data}])
        print ('notifications pro',notifications)
        self.env['bus.bus'].sendmany(notifications)

class product_product(models.Model):
    _inherit = 'product.product'

    @api.model
    def create(self, values):
        print ('create pp')
        res = super(product_product, self).create(values)
        self.env['pos.config'].sync_order_data(res.id)
        return res

    @api.multi
    def write(self, vals):
        print ('write pp')
        result = super(product_product, self).write(vals)
        return result

        lst=[]
        for re in self:
            # for al_da in allo_data:
            # self.env['pos.config'].sync_order_data(re.id)
            lst.append(re.id)
        self.env['pos.config'].sync_order_data_list(lst)
        return result

    # @api.one
    # @api.depends('qty_available')
    # def _compute_qty_available(self):
    #     for re in self:
    #         # for al_da in allo_data:
    #         self.env['pos.config'].sync_order_data(re.id)
    #
    # @api.onchange('qty_available')
    # def _compute_qty_available1(self):
    #     for re in self:
    #         # for al_da in allo_data:
    #         self.env['pos.config'].sync_order_data(re.id)
    #

class product_template(models.Model):
    _inherit = 'product.template'

    @api.multi
    def write(self, vals):
        print ('write pt')

        result = super(product_template, self).write(vals)
        for re in self:
            product_ids = self.env['product.product'].search([('product_tmpl_id','=',re.id)])
            for pid in product_ids.ids:
                self.env['pos.config'].sync_order_data(pid)
        return result

    # @api.multi
    # @api.depends('qty_available')
    # def _compute_qty_available(self):
    #     for re in self:
    #         # for al_da in allo_data:
    #         self.env['pos.config'].sync_order_data(re.id)
    #
    # @api.onchange('qty_available')
    # def _compute_qty_available1(self):
    #     for re in self:
    #         # for al_da in allo_data:
    #         self.env['pos.config'].sync_order_data(re.id)
    #




