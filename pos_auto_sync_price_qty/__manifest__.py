# -*- coding: utf-8 -*-

{
    'name': 'pos_auto_sync_price_qty',
    'version': '1.0',
    'category': 'Point of Sale',
    'author': 'Nova Minds',
    'website': 'www.nmcit.com',
    'sequence': 6,
    'summary': 'synchronize quantity and price of pos in different sessions.',
    'description': """

 The purpose of this module is to auto synchronize the session of point of sales(POS) with the operations of other modules in case of updating price , updating quantity and updating product name to make the session up to date. 

""",
    'depends': ['point_of_sale','stock','pos_picking_fixed_issues'],
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml'
    ],
    'qweb': [
        # 'static/src/xml/pos.xml',
    ],
    'images': [
        'static/description/pos_view.jpg',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'price': 10,
    'license': 'LGPL-3',
    'currency': 'EUR',
}
