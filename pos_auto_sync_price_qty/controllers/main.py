# -*- coding: utf-8 -*-

from odoo.http import Controller, request, route
import logging
_logger = logging.getLogger(__name__)


try:
    from odoo.addons.bus.controllers.main import BusController
except ImportError:
    _logger.error('Error')
    BusController = object


class Controller(BusController):

    def _poll(self, dbname, channels, last, options):
        if request.session.uid:
            channels.append((request.db, 'wv_pos_sync_data_sess', request.uid))
        return super(Controller, self)._poll(dbname, channels, last, options)

