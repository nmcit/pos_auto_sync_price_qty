odoo.define('pos_auto_sync_data', function(require){
    var exports = {};
    var Backbone = window.Backbone;
    var screens = require('point_of_sale.screens');
    var models = require('point_of_sale.models');
    var bus = require('bus.bus');

    screens.OrderWidget.include({
        rerender_orderline: function(order_line){
            if (order_line.node) {return this._super(order_line);}
        },
        remove_orderline: function(order_line){
            if(order_line.node){
                if (order_line.node.parentNode) {
                    return this._super(order_line);
                }
            }
            if(this.pos.get('selectedOrder').get('orderLines')) {
                if(this.pos.get('selectedOrder').get('orderLines').length ==0){return this._super(order_line);}
            }
        }
    });
  
    var PosModelSuper = models.PosModel;
    models.PosModel = models.PosModel.extend({
        load_server_data: function () {
            res = PosModelSuper.prototype.load_server_data.apply(this);
            var self = this;
            return res.then(function () {
                if (self.config.allow_incoming_orders) {
                    self.sync_auto_data_session = new exports.SyncAutoData(self);
                    self.sync_auto_data_session.start();
                }
            });
        },
    });

   
    exports.SyncAutoData = Backbone.Model.extend({
        initialize: function(pos){
            this.pos = pos;
        },
        start: function(){
            var self = this;
            this.bus = bus.bus;
            this.bus.last = this.pos.db.load('bus_last', 0);
            this.bus.on("notification", this, this.bus_notification);
            this.bus.start_polling();
        },
        
        bus_notification: function(bus_message) {
            console.log(bus_message);
            var self = this;
            var def  = new $.Deferred();
            if(bus_message[0][0]){
                if(bus_message[0][0][1] === "wv_pos_sync_data_sess"){
                self.pos.db.add_products(bus_message[0][1].data);
                this.pos.gui.screen_instances['products'].replace();
                def.resolve();
                }
            }
        },
    });
});
